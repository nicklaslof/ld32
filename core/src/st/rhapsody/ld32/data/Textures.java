package st.rhapsody.ld32.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class Textures {

    public static TextureAtlas textureAtlas;
    public static Texture starField;

    public static void init(TextureAtlas atlas, Texture starField){

        Textures.textureAtlas = atlas;


        Textures.starField = starField;
        Textures.starField.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
    }
}
