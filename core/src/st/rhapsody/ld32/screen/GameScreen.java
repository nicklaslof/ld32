package st.rhapsody.ld32.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import st.rhapsody.ld32.Audio;
import st.rhapsody.ld32.LD32;
import st.rhapsody.ld32.level.Level;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class GameScreen implements Screen {
    private final LD32 ld32;
    private final boolean freeplay;
    private OrthographicCamera camera;
    private FitViewport viewport;
    private SpriteBatch spriteBatch;
    private Level level;
    public static int width = 640;
    public static int height = 480;
    private SpriteBatch uiBatch;
    private GlyphLayout layout;
    private BitmapFont font;
    private boolean dispose;

    public GameScreen(LD32 ld32, boolean freeplay) {

        this.ld32 = ld32;
        this.freeplay = freeplay;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera();
        viewport = new FitViewport(width, height, camera);
        spriteBatch = new SpriteBatch();
        uiBatch = new SpriteBatch();
        createLevel();
        font = new BitmapFont();

        layout = new GlyphLayout();
    }

    private void createLevel() {
        level = new Level();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glClearColor(0, 0, 0, 0);

        if (dispose){
            return;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.R)){
            createLevel();
            return;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.M)){
            Audio.switchMode();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.F)){
            if (!Gdx.graphics.isFullscreen()) {
                Gdx.graphics.setDisplayMode(width, height, true);
            }else{
                Gdx.graphics.setDisplayMode(width, height, false);
            }
        }

        if (!freeplay && level.getPlayerScore() > 2999){
            dispose = true;
            ld32.switchToEnd();
            return;
        }


        tick(delta);

        spriteBatch.begin();

        renderSprites(spriteBatch);

        spriteBatch.end();


        uiBatch.begin();
        layout.setText(font,"Score: "+ level.getPlayerScore() +"   Health: "+Math.max(0, level.getPlayerHealth()));
        font.draw(uiBatch, layout, 0, height);

        if (level.getPlayerHealth() <= 0){
            layout.setText(font,"You died! Press R to start again");
            font.draw(uiBatch, layout, (width/2)-100f, (height/2));
        }


        uiBatch.end();


        Gdx.graphics.setTitle("FPS:" + Gdx.graphics.getFramesPerSecond());

    }

    private void tick(float delta) {
        level.tick(delta);
    }

    private void renderSprites(SpriteBatch spriteBatch) {
        level.render(spriteBatch);
    }

    @Override
    public void resize(int width, int height) {
       // viewport.setScreenSize(width,height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        uiBatch.dispose();
    }
}
