package st.rhapsody.ld32.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import st.rhapsody.ld32.Audio;
import st.rhapsody.ld32.LD32;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.graphic.Starfield;
import st.rhapsody.ld32.level.Level;

/**
 * Created by nicklaslof on 19/04/15.
 */
public class MenuScreen implements Screen {
    private final LD32 ld32;
    private OrthographicCamera camera;
    private FitViewport viewport;
    private SpriteBatch spriteBatch;
    private Level level;
    public static int width = 640;
    public static int height = 480;
    private SpriteBatch uiBatch;
    private GlyphLayout layout;
    private BitmapFont font;
    private Starfield starfield;
    private Sprite bananaLogo;

    private Array<Sprite> bananas = new Array<Sprite>();
    private float time;
    private float continueCountdown = 2f;
    private boolean dispose;

    public MenuScreen(LD32 ld32) {

        this.ld32 = ld32;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera();
        viewport = new FitViewport(width, height, camera);
        spriteBatch = new SpriteBatch();
        uiBatch = new SpriteBatch();

        font = new BitmapFont();

        layout = new GlyphLayout();


        starfield = new Starfield();





    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glClearColor(0, 0, 0, 0);

        if (dispose){
            return;
        }

        continueCountdown -= delta;

        if (continueCountdown < 0 && Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            dispose = true;
            ld32.switchToIntro();
            return;
        }

        if (continueCountdown < 0 && Gdx.input.isKeyJustPressed(Input.Keys.F)){
            dispose = true;
            ld32.switchToGame(true);
            return;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.M)){
            Audio.switchMode();
        }



        starfield.tick(delta);


        uiBatch.begin();

        layout.setText(font, "Lets go");
        font.draw(uiBatch, layout, (width / 2) - 10, (height / 2) + 205);

        layout.setText(font, "In outer space");
        font.draw(uiBatch, layout, (width / 2) - 30, (height / 2) + 125);


        layout.setText(font, "Game made for the Ludum Dare 32 compo");
        font.draw(uiBatch, layout, (width / 2) - 120, (height / 2) + 55);

        layout.setText(font, "By Nicklas Löf");
        font.draw(uiBatch, layout, (width / 2) - 30, (height / 2) + 25);


        layout.setText(font, "WASD to steer, Space to shoot, R to quickstart, M to mute sounds and music");
        font.draw(uiBatch, layout, (width / 2) - 230, (height / 2) - 85);

        layout.setText(font, "Space to start - or F to start free play without the story and ending");
        font.draw(uiBatch, layout, (width / 2) - 200, (height / 2) - 125);

        layout.setText(font, "F (in game) switch to fullscreen");
        font.draw(uiBatch, layout, (width / 2) - 100, (height / 2) - 165);


        uiBatch.end();


        spriteBatch.begin();

        starfield.render(spriteBatch);


        Sprite banana = bananas.get(0);


        banana.setX((width / 2) - 200);
        banana.setY(height / 2);
        banana.rotate(delta * 300);
        banana.draw(spriteBatch);



        banana = bananas.get(1);


        banana.setX((width/ 2)+200);
        banana.setY(height / 2);
        banana.rotate(-delta*300);
        banana.draw(spriteBatch);



        bananaLogo.setX((width / 2)-50);
        bananaLogo.setY((height/2)+150);

        bananaLogo.draw(spriteBatch);


        spriteBatch.end();

    }

    @Override
    public void resize(int width, int height) {

        bananaLogo = Textures.textureAtlas.createSprite("entity/bananaslogo");
        bananaLogo.scale(3f);

        font = new BitmapFont();


        bananas = new Array<Sprite>();


        for (int i = 0; i < 2; i++) {
            Sprite sprite = new Sprite(Textures.textureAtlas.createSprite("entity/banana"));
            sprite.setScale(2f);

            bananas.add(sprite);
        }


        new GlyphLayout();

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        uiBatch.dispose();
    }
}
