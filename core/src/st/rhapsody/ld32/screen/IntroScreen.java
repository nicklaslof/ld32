package st.rhapsody.ld32.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import st.rhapsody.ld32.Audio;
import st.rhapsody.ld32.LD32;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.graphic.Starfield;
import st.rhapsody.ld32.level.Level;

/**
 * Created by nicklaslof on 19/04/15.
 */
public class IntroScreen implements Screen {

    private final LD32 ld32;
    private OrthographicCamera camera;
    private FitViewport viewport;
    private SpriteBatch spriteBatch;
    private Level level;
    public static int width = 640;
    public static int height = 480;
    private SpriteBatch uiBatch;
    private GlyphLayout layout;
    private BitmapFont font;
    private Starfield starfield;
    private Sprite bananaLogo;

    private float continueCountdown = 2;
    private boolean dispose;
    private Sprite cherrySprite;
    private Sprite particleSprite;
    private Sprite pumpkinSprite;

    public IntroScreen(LD32 ld32) {

        this.ld32 = ld32;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera();
        viewport = new FitViewport(width, height, camera);
        spriteBatch = new SpriteBatch();
        uiBatch = new SpriteBatch();

        font = new BitmapFont();

        layout = new GlyphLayout();


        starfield = new Starfield();


        cherrySprite = Textures.textureAtlas.createSprite("entity/cherry");
        cherrySprite.setScale(3f);
        particleSprite = Textures.textureAtlas.createSprite("entity/particle");
        particleSprite.setScale(0.5f);
        pumpkinSprite = Textures.textureAtlas.createSprite("entity/pumpkin");
        pumpkinSprite.setScale(3f);



    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glClearColor(0, 0, 0, 0);

        if (dispose){
            return;
        }

        continueCountdown -= delta;

        if (continueCountdown < 0 && Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            dispose = true;
            ld32.switchToGame(false);
            return;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.M)){
            Audio.switchMode();
        }


        starfield.tick(delta);


        uiBatch.begin();

        layout.setText(font, "It's a normal day in space. You are just on your way home from the planet Quita.");
        font.draw(uiBatch, layout, (width / 2) - 300, (height / 2) + 205);

        layout.setText(font, "Suddenly you see a horde of various space alien pirates in strange ships");
        font.draw(uiBatch, layout, (width / 2) - 300, (height / 2) + 175);


        layout.setText(font, "You are out of power for the laser canons so there is only one possibility to survive!");
        font.draw(uiBatch, layout, (width / 2) - 300, (height / 2) + 145);

        layout.setText(font, "By using the cargo you have picked up on Quita.");
        font.draw(uiBatch, layout, (width / 2) - 300, (height / 2) + 115);

        layout.setText(font, "So in desperation you start to fire bananas at your enemies.");
        font.draw(uiBatch, layout, (width / 2) - 300, (height / 2) + 85);


        layout.setText(font, "Are you ready? If so press space!");
        font.draw(uiBatch, layout, (width / 2) - 300, (height / 2) - 185);



        layout.setText(font, "Health!");
        font.draw(uiBatch, layout, 70, (height / 2)+20);

        layout.setText(font, "Watch out for these!");
        font.draw(uiBatch, layout, 70, (height / 2)-40);

        layout.setText(font, "20 seconds random banana upgrade!");
        font.draw(uiBatch, layout, 70, (height / 2)-100);



        uiBatch.end();


        spriteBatch.begin();

        starfield.render(spriteBatch);


        cherrySprite.setX((30));
        cherrySprite.setY((height / 2)+5);
        cherrySprite.rotate(delta * 300);

        cherrySprite.draw(spriteBatch);

        particleSprite.setX(30);
        particleSprite.setY((height / 2)-55);
        particleSprite.rotate(delta * 300);

        particleSprite.draw(spriteBatch);


        pumpkinSprite.setX(30);
        pumpkinSprite.setY((height / 2)-115);
        pumpkinSprite.rotate(delta * 300);

        pumpkinSprite.draw(spriteBatch);


        spriteBatch.end();


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        uiBatch.dispose();
    }
}
