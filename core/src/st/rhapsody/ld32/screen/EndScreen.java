package st.rhapsody.ld32.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import st.rhapsody.ld32.LD32;
import st.rhapsody.ld32.graphic.Starfield;
import st.rhapsody.ld32.level.Level;

/**
 * Created by nicklaslof on 19/04/15.
 */
public class EndScreen implements Screen {


    private final LD32 ld32;
    private OrthographicCamera camera;
    private FitViewport viewport;
    private SpriteBatch spriteBatch;
    public static int width = 640;
    public static int height = 480;
    private SpriteBatch uiBatch;
    private GlyphLayout layout;
    private BitmapFont font;
    private Starfield starfield;
    private Sprite bananaLogo;
    private float continueCountdown = 2f;
    private boolean dispose;


    public EndScreen(LD32 ld32) {
        this.ld32 = ld32;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera();
        viewport = new FitViewport(width, height, camera);
        spriteBatch = new SpriteBatch();
        uiBatch = new SpriteBatch();

        font = new BitmapFont();

        layout = new GlyphLayout();


        starfield = new Starfield();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glClearColor(0, 0, 0, 0);

        if (dispose){
            return;
        }

        continueCountdown -= delta;

        if (continueCountdown < 0 && Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            dispose = true;
            ld32.switchToMenu();
            return;
        }


        starfield.tick(delta);


        uiBatch.begin();


        layout.setText(font, "You have arrived home safely! (Arrived at score 3000)");
        font.draw(uiBatch, layout, (width / 2) - 300, (height / 2) + 205);

        layout.setText(font, "Now you have to tell your boss what did happen to the big delivery of bananas");
        font.draw(uiBatch, layout, (width / 2) - 300, (height / 2) + 175);


        layout.setText(font, "But that is another story");
        font.draw(uiBatch, layout, (width / 2) - 300, (height / 2) + 145);




        uiBatch.end();


        spriteBatch.begin();

        starfield.render(spriteBatch);


        spriteBatch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
