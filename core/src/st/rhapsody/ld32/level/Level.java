package st.rhapsody.ld32.level;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.entity.*;
import st.rhapsody.ld32.entity.behaviour.*;
import st.rhapsody.ld32.graphic.Starfield;
import st.rhapsody.ld32.screen.GameScreen;

import java.util.Iterator;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class Level {


    private static Array<Entity> entities;
    private static Array<Entity> bullets;
    private static Array<Entity> particles;



    private static Entity player;
    private static Formation formation;
    private final Starfield starfield;
    private int formationsSinceBoss;
    private int totalFormations;

    private static int playerScore = 0;


    public Level() {

        entities  = new Array<Entity>();
        bullets = new Array<Entity>();
        particles = new Array<Entity>();
        formation = new Formation();
        playerScore = 0;
        createFormation(getRandomFormation());

        Entity entity = new Entity(new Vector2(0,GameScreen.height/2f))
                .addBehaviour(new PlayerControlled())
                .addRenderer(new SpriteRender(Textures.textureAtlas.createSprite("entity/playership"),2f, Color.WHITE))
                .setIsEnemy(false)
                .setHealth(10);
        player = entity;

        entities.add(entity);

        starfield = new Starfield();


    }

    private void createFormation(Formation.Type formationType) {

        if (formation.hasBoss()){
            return;
        }

        switch(formationType){

            case SWARM:
                for (int i = 1; i < 11; i++) {
                    Entity entity = new Entity(new Vector2((GameScreen.width+50)+ MathUtils.random(-50, 50),(GameScreen.height/2)+MathUtils.random(-50,50)))
                            .addBehaviour(new SwarmpatternBehaviour().setParticleColor(new Color(0x7d62c4ff)).setTimeOffset(i / MathUtils.random(5f, 10f)))
                            .addRenderer(new SpriteRender(Textures.textureAtlas.createSprite("entity/enemycircle"),2f, Color.WHITE))
                            .setHealth(2);

                    entities.add(entity);
                    formation.add(entity);
                }

                break;

            case LINE: {
                    float startY = MathUtils.random(16, GameScreen.height-16);

                    for (int i = 1; i < 11; i++) {
                        Sprite sprite = Textures.textureAtlas.createSprite("entity/playership");
                        sprite.setFlip(true,false);
                        Entity entity = new Entity(new Vector2((GameScreen.width) + 32 * i, startY))
                                .addBehaviour(new LineBehaviour().setParticleColor(new Color(0xff7c76ff)))
                                .addRenderer(new SpriteRender(sprite, 2f, new Color(0xff7c76ff)))
                                .setHealth(2);

                        entities.add(entity);
                        formation.add(entity);
                    }
                }
                break;

            case LINESWEEP: {
                    float startY = MathUtils.random(200, GameScreen.height-200);

                    float sweepsize = MathUtils.random(100,400);

                    for (int i = 1; i < 11; i++) {
                        Entity entity = new Entity(new Vector2((GameScreen.width) + 32 * i, startY))
                                .addBehaviour(new LineSweepBehaviour().setTimeOffset(i/10f).setSweepsize(sweepsize).setParticleColor(new Color(0x7d62c4ff)))
                                .addRenderer(new SpriteRender(Textures.textureAtlas.createSprite("entity/enemycircle"), 2f, Color.WHITE))
                                .setHealth(2);

                        entities.add(entity);
                        formation.add(entity);
                    }
                }
                break;

            case MINIBOSS: {

                float startY = MathUtils.random(200, GameScreen.height-200);

                float sweepsize = MathUtils.random(100,400);

                Entity entity = new Entity(new Vector2((GameScreen.width) + 32, startY))
                        .addBehaviour(new RotatingBehaviour().setSweepsize(sweepsize).setParticleColor(new Color(0x7d62c4ff)))
                        .addRenderer(new SpriteRender(Textures.textureAtlas.createSprite("entity/enemycircle"), 4f, new Color(0xff7c76ff)))
                        .setHealth(5);

                entities.add(entity);
                formation.add(entity);
                formation.setHasBoss(true);

            }

            case MINIBOSS2: {

                float startY = MathUtils.random(200, GameScreen.height-200);

                float sweepsize = MathUtils.random(100,400);

                Entity entity = new Entity(new Vector2((GameScreen.width) + 32, startY))
                        .addBehaviour(new Rotating2Behaviour().setSweepsize(sweepsize).setParticleColor(new Color(0x7d62c4ff)))
                        .addRenderer(new SpriteRender(Textures.textureAtlas.createSprite("entity/enemycircle"), 4f, new Color(0xff7c76ff)))
                        .setHealth(8);

                entities.add(entity);
                formation.add(entity);
                formation.setHasBoss(true);

            }



        }


    }

    public static Vector2 getPlayerPosition() {
        return player.getPosition();
    }

    public void tick(float delta) {

        starfield.tick(delta);


       tickEntities(entities, delta);


       tickEntities(bullets, delta);

        tickEntities(particles, delta);

        checkCollission();

        if (formation.isDone()){
            formation = new Formation();

            int random = MathUtils.random(20);
            System.out.println("new formations random: "+random +"   total:"+totalFormations);
            if (random < totalFormations){
                createFormation(getRandomFormation());
                if (!formation.hasBoss()) {
                    createFormation(getRandomFormation());
                }
            }else{

                createFormation(getRandomFormation());
            }

        }

    }


    private void checkCollission(){
        for (Entity entity : entities) {
            for (Entity bullet : bullets) {
                if (bullet.getPosition().dst(entity.getPosition()) < 32f){
                    if (bullet.collidesWith(entity)){
                        bullet.onCollision(entity);
                    }
                }
            }
            for (Entity particle : particles) {
                if (particle.getPosition().dst(entity.getPosition()) < 32f){
                    if (particle.collidesWith(entity)){
                        particle.onCollision(entity);
                    }
                }
            }

        }


    }

    private void tickEntities(Array<Entity> entitiesToTick, float delta){
        Iterator<Entity> iterator = entitiesToTick.iterator();

        while(iterator.hasNext()){
            Entity entity = iterator.next();
            entity.tick(delta);
            if (entity.isDisposable()){
                formation.remove(entity);
                iterator.remove();
            }
        }
    }

    public void render(SpriteBatch spriteBatch) {



        starfield.render(spriteBatch);


        for (Entity entity : entities) {
            entity.render(spriteBatch);
        }

        for (Entity bullet : bullets) {
            bullet.render(spriteBatch);
        }

        for (Entity particle : particles) {
            particle.render(spriteBatch);
        }
    }

    public static void shoot(Entity shooter, Vector2 src, Vector2 direction, float velocity, Sprite sprite, boolean enemyBullet) {
        Entity bullet = new Entity(src).addBehaviour(new BulletBehaviour(shooter,src, direction, velocity, enemyBullet)).addRenderer(new SpriteRender(sprite,2f, Color.WHITE));

        bullets.add(bullet);
    }

    public static void addParticle(Entity shooter, Vector2 src, Vector2 direction, float velocity, Sprite sprite, float ttl, Color color){

        Entity particle = new Entity(src).addBehaviour(new ParticleBehaviour(shooter, src, direction, velocity, ttl)).addRenderer(new SpriteRender(sprite,0.5f,color));

        particles.add(particle);

    }

    public static void addPowerup(boolean health, Entity shooter, Vector2 src, Vector2 direction, float velocity, Sprite sprite, float ttl, Color color){

        Entity particle = new Entity(src).addBehaviour(new PowerupBehaviour(health,shooter, src, direction, velocity, ttl)).addRenderer(new SpriteRender(sprite,2.5f,color));

        particles.add(particle);

    }

    public static void increasePlayerScore(int score){
        playerScore += score;
    }

    public int getPlayerHealth() {
        return player.getHealth();
    }

    public static int getPlayerScore() {
        return playerScore;
    }

    public Formation.Type getRandomFormation() {
        totalFormations++;
        formationsSinceBoss++;

        if (formationsSinceBoss > 5){
            int r = MathUtils.random(20);
            System.out.println("checking if time for miniboss:   " + r);
            if (r < 10){
                formationsSinceBoss = 0;
                if (r < 4f){
                    return Formation.Type.MINIBOSS2;
                }else{
                    return Formation.Type.MINIBOSS;
                }


            }
        }


        int random = MathUtils.random(0, 2);
        switch (random) {
            case 0:
                return Formation.Type.SWARM;
            case 1:
                return Formation.Type.LINE;
            case 2:
                return Formation.Type.LINESWEEP;
        }

        return Formation.Type.SWARM;
    }
}
