package st.rhapsody.ld32.graphic;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.screen.GameScreen;

/**
 * Created by nicklaslof on 19/04/15.
 */
public class Starfield {
    private static Sprite starsLayer1;
    private static Sprite starsLayer2;
    private static Sprite starsLayer3;

    public Starfield() {
        starsLayer1 = new Sprite(Textures.starField);
        starsLayer1.setSize(GameScreen.width, GameScreen.height);
        starsLayer2 = new Sprite(Textures.starField);
        starsLayer2.setSize(GameScreen.width, GameScreen.height);
        starsLayer2.setAlpha(0.4f);
        starsLayer3 = new Sprite(Textures.starField);
        starsLayer3.setSize(GameScreen.width,GameScreen.height);
        starsLayer3.setAlpha(0.2f);
    }


    public void tick(float delta){

        starsLayer1.scroll(delta/2.5f,0);
        starsLayer2.scroll(delta / 3.0f, 0);
        starsLayer3.scroll(delta / 4.0f, 0);
    }

    public void render(SpriteBatch spriteBatch){
        starsLayer3.draw(spriteBatch);
        starsLayer2.draw(spriteBatch);
        starsLayer1.draw(spriteBatch);
    }
}
