package st.rhapsody.ld32.entity;

import com.badlogic.gdx.utils.Array;


/**
 * Created by nicklaslof on 18/04/15.
 */
public class Formation {

    private boolean hasBoss = false;

    public boolean hasBoss() {
        return hasBoss;
    }

    public void setHasBoss(boolean hasBoss) {
        this.hasBoss = hasBoss;
    }

    public enum Type{

        SWARM(1),
        LINE(2),
        LINESWEEP(3),
        MINIBOSS(4),
        MINIBOSS2(5);

        int id;

        Type(int id) {
            this.id = id;
        }

    }



    private Array<Entity> entities = new Array<Entity>();

    public void add(Entity entity) {
       // System.out.println("adding "+entity);
        entities.add(entity);

    }


    public void remove(Entity entity){
        entities.removeValue(entity, true);
      //  System.out.println("size after removal: " + entities.size);
    }


    public boolean isDone() {
        return entities.size <= 0;
    }
}
