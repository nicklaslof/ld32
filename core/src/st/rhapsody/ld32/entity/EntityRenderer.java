package st.rhapsody.ld32.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by nicklaslof on 18/04/15.
 */
public abstract class EntityRenderer {

    public abstract void render(SpriteBatch spriteBatch, float x, float y, float rotationDegrees);
}
