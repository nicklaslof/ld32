package st.rhapsody.ld32.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld32.entity.behaviour.EntityBehaviour;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class Entity {

    private EntityRenderer entityRenderer;
    private EntityBehaviour entityBehaviour;

    private Rectangle rectangle = new Rectangle();
    private boolean isEnemy = true;

    private float x;
    private float y;

    private float rotationDegrees;

    private int health = 5;

    private Vector2 position = new Vector2();
    private boolean disposable;

    public Entity(Vector2 src) {
        x = src.x+1;
        y = src.y;
        rectangle.set(x,y,24,24);
    }

    public Entity() {
        x = 1;
        y = 1;
        rectangle.set(x, y, 24, 24);
    }

    public void tick(float delta) {
        if (health <= 0){
            disposable = true;
            entityBehaviour.onDispose(this);
            return;
        }
        entityBehaviour.tick(delta, this);
    }

    public void hit(){
        //System.out.println(health);
        boolean canBeHit = entityBehaviour.onHit(this, health);
        if (canBeHit){
            health--;
        }
    }

    public Entity setIsEnemy(boolean isEnemy) {
        this.isEnemy = isEnemy;
        return this;
    }

    public Entity setHealth(int health) {
        this.health = health;
        return this;
    }

    public void translate(float tx, float ty){
        x += tx;
        y += ty;

        rectangle.set(x, y, 24, 24);
    }

    public void rotate(float degrees){
        rotationDegrees += degrees;
    }

    public void render(SpriteBatch spriteBatch) {

        entityRenderer.render(spriteBatch, x, y, rotationDegrees);


    }

    public Entity addBehaviour(EntityBehaviour entityBehaviour) {
        this.entityBehaviour = entityBehaviour;
        return this;
    }

    public Entity addRenderer(EntityRenderer entityRenderer) {
        this.entityRenderer = entityRenderer;
        return this;
    }

    public Vector2 getPosition() {
        position.set(x,y);
        return position;
    }

    public void setPosition(float x, float y){
        this.x = x;
        this.y = y;

        rectangle.set(x, y, 16, 16);
    }

    public int getHealth() {
        return health;
    }

    public boolean isDisposable() {
        return disposable;
    }

    public void setDisposable(boolean disposable) {
        this.disposable = disposable;
    }

    public boolean collidesWith(Entity entity) {
        if (disposable){
            return false;
        }
        if (entity.rectangle.overlaps(this.rectangle)){
           return true;
        }
        return false;
    }

    public void onCollision(Entity entity) {
        entityBehaviour.onCollision(entity);
    }

    @Override
    public String toString() {
        return "Entity{" +
                "entityRenderer=" + entityRenderer +
                ", entityBehaviour=" + entityBehaviour +
                ", rectangle=" + rectangle +
                ", x=" + x +
                ", y=" + y +
                ", position=" + position +
                ", disposable=" + disposable +
                '}';
    }

    public boolean isEnemy() {
        return isEnemy;
    }

    public void heal() {
        health++;
    }

    public void powerup() {
        entityBehaviour.powerup();
    }
}
