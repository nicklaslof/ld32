package st.rhapsody.ld32.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class SpriteRender extends EntityRenderer{

    private final Sprite sprite;

    public SpriteRender(Sprite sprite, float scale, Color color) {

        this.sprite = sprite;
        this.sprite.setScale(scale);
        sprite.setColor(color);
    }

    @Override
    public void render(SpriteBatch spriteBatch, float x, float y, float rotationDegrees) {
        sprite.setRotation(rotationDegrees);
        sprite.setPosition(x,y);
        sprite.draw(spriteBatch);
    }
}
