package st.rhapsody.ld32.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld32.Audio;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.entity.behaviour.EntityBehaviour;
import st.rhapsody.ld32.level.Level;
import st.rhapsody.ld32.screen.GameScreen;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class PlayerControlled extends EntityBehaviour {
    private boolean backwardShooter;
    private float shooterPowerupTimer;
    private boolean doubleShooter;
    private float hitCooldown;

    @Override
    public void doTick(float delta, Entity entity) {

        if (shooterPowerupTimer > 0) {
            shooterPowerupTimer -= delta;
        }

        if (hitCooldown > 0){
            hitCooldown -= delta;
        }

        float velocity = delta*280f;

        Vector2 currentPosition = entity.getPosition();

        if (Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP)){
            if (currentPosition.y < GameScreen.height-32) {
                entity.translate(0, +velocity);
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.S) || Gdx.input.isKeyPressed(Input.Keys.DOWN)){
            if (currentPosition.y > 16) {
                entity.translate(0, -velocity);
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            if (currentPosition.x > 0) {
                entity.translate(-velocity, 0);
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            if (currentPosition.x < GameScreen.width) {
                entity.translate(+velocity, 0);
            }
        }


        if (entity.getPosition().x < 0){
            entity.setPosition(0, entity.getPosition().y);
        }

        if (entity.getPosition().y < 0) {
            entity.setPosition(entity.getPosition().x, 0);
        }

        if (shooterPowerupTimer <= 0){
            backwardShooter = false;
            doubleShooter = false;
        }


        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
           // System.out.println("shooting from "+entity.getPosition());




            if (doubleShooter){
                Level.shoot(entity, entity.getPosition().cpy().add(0,15),new Vector2(1,0),800f, Textures.textureAtlas.createSprite("entity/banana"), false);
                Level.shoot(entity, entity.getPosition().cpy().add(0,-15),new Vector2(1,0),800f, Textures.textureAtlas.createSprite("entity/banana"), false);
            }else if (backwardShooter){
                Level.shoot(entity, entity.getPosition(),new Vector2(-1,0),800f, Textures.textureAtlas.createSprite("entity/banana"), false);
                Level.shoot(entity, entity.getPosition(),new Vector2(1,0),800f, Textures.textureAtlas.createSprite("entity/banana"), false);
            }else{
                Level.shoot(entity, entity.getPosition(),new Vector2(1,0),800f, Textures.textureAtlas.createSprite("entity/banana"), false);
            }

            Audio.playSound(Audio.laser, 0.3f);


        }


    }

    @Override
    public void onCollision(Entity entity) {

    }

    @Override
    public void onDispose(Entity entity) {

    }

    @Override
    public boolean onHit(Entity entity, int health) {
        if (hitCooldown > 0.0f){
            return false;
        }
        Level.addParticle(entity, entity.getPosition(), new Vector2(MathUtils.random(-1f, 1f), MathUtils.random(-1f, 1f)), MathUtils.random(50, 150), Textures.textureAtlas.createSprite("entity/particle"), 1, new Color(0xffe597ff));
        Audio.playSound(Audio.hit, 0.6f);
        hitCooldown = 0.2f;
        return true;
    }

    @Override
    public void powerup() {
        int random = MathUtils.random(30);
        backwardShooter = false;
        doubleShooter = false;
        if (random < 15) {
            backwardShooter = true;
        }else{
            doubleShooter = true;
        }

        shooterPowerupTimer = 25.0f;
    }
}
