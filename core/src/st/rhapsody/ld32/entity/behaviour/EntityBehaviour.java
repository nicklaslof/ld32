package st.rhapsody.ld32.entity.behaviour;

import st.rhapsody.ld32.entity.Entity;

/**
 * Created by nicklaslof on 18/04/15.
 */
public abstract class EntityBehaviour {
    boolean dispose;

    public void tick(float delta, Entity entity){

        doTick(delta, entity);

        if (dispose){
            entity.setDisposable(dispose);
        }
    }


    public abstract void doTick(float delta, Entity entity);

    public abstract void onCollision(Entity entity);

    public abstract void onDispose(Entity entity);

    public abstract boolean onHit(Entity entity, int health);


    public abstract void powerup();
}
