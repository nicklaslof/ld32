package st.rhapsody.ld32.entity.behaviour;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.entity.Entity;
import st.rhapsody.ld32.level.Level;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class Rotating2Behaviour extends EnemyBehaviour{

    private float timeOffset;
    private float frequency;
    private float time;
    private float bulletTime = 2;
    private float sweepSize;
    private int bulletCounter;
    private float bulletPause;

    private Vector2 tmpVector = new Vector2();

    public EnemyBehaviour setTimeOffset(float timeOffset){
        this.timeOffset = timeOffset;
        return this;
    }

    public EnemyBehaviour setSweepsize(float sweepSize){
        this.sweepSize = sweepSize;
        return this;
    }

    @Override
    public void doTick(float delta, Entity entity) {
        super.doTick(delta, entity);

        bulletTime -= delta;
        bulletPause -= delta;

        frequency = 0.5f;

        time += delta;

        float sin = MathUtils.sin(((time + timeOffset) * MathUtils.PI2) * frequency);

        entity.translate(-(delta * 70f), (sin * (delta * 150f)));
        entity.rotate(delta*300);

        if (bulletTime <= 0) {
            bulletCounter = 36;
            bulletTime = 3;
        }

        if (bulletPause <= 0 && bulletCounter > 0){
                tmpVector.set(MathUtils.sin(bulletCounter * 10f), MathUtils.cos(bulletCounter * 10f));
                Level.shoot(entity, entity.getPosition(), tmpVector.nor(), 300f, Textures.textureAtlas.createSprite("entity/chili"), true);
            bulletCounter--;
            bulletPause = 0.015f;
        }
    }

    @Override
    public void onDispose(Entity entity) {
        super.onDispose(entity);
        Level.increasePlayerScore(30);
    }
}