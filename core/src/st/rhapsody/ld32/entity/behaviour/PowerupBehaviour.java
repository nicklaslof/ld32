package st.rhapsody.ld32.entity.behaviour;

import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld32.Audio;
import st.rhapsody.ld32.entity.Entity;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class PowerupBehaviour extends EntityBehaviour {


    private final boolean health;

    public PowerupBehaviour(boolean health, Entity shooter, Vector2 src, Vector2 direction, float velocity, float ttl) {
        super();
        this.health = health;
    }

    @Override
    public void doTick(float delta, Entity entity) {

        entity.translate(-(delta * 100f), 0);
    }

    @Override
    public void onCollision(Entity entity) {
        if (!entity.isEnemy()){
            if (health) {
                entity.heal();
            }else{
                entity.powerup();
            }
            Audio.playSound(Audio.powerup, 0.6f);
            dispose = true;
        }
    }

    @Override
    public void onDispose(Entity entity) {

    }

    @Override
    public boolean onHit(Entity entity, int health) {
        return true;
    }

    @Override
    public void powerup() {

    }
}
