package st.rhapsody.ld32.entity.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld32.Audio;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.entity.Entity;
import st.rhapsody.ld32.level.Level;
import st.rhapsody.ld32.screen.GameScreen;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class EnemyBehaviour extends EntityBehaviour{

    private Color particleColor;


    @Override
    public void doTick(float delta, Entity entity) {
        Vector2 position = entity.getPosition();
        if (position.x < -300 || position.x > GameScreen.width+300 || position.y < -300 || position.y > GameScreen.height+300){
            dispose = true;
            return;
        }
    }

    public EnemyBehaviour setParticleColor(Color particleColor) {
        this.particleColor = particleColor;
        return this;
    }

    @Override
    public void onCollision(Entity entity) {
        if (!entity.isEnemy()){
            entity.hit();
        }
    }

    @Override
    public void onDispose(Entity entity) {
        for (int i = 0; i <  MathUtils.random(5,20); i++) {
            Level.addParticle(entity, entity.getPosition(), new Vector2(MathUtils.random(-1f,1f), MathUtils.random(-1f,1f)), MathUtils.random(50,150), Textures.textureAtlas.createSprite("entity/particle"), 1, particleColor);

        }

        int random = MathUtils.random(30);

        if (random < 10){
            if (random < 5) {
                Level.addPowerup(true, entity, entity.getPosition(), new Vector2(MathUtils.random(-1f, 1f), MathUtils.random(-1f, 1f)), MathUtils.random(50, 150), Textures.textureAtlas.createSprite("entity/cherry"), 5, Color.WHITE);
            }else{
                Level.addPowerup(false, entity, entity.getPosition(), new Vector2(MathUtils.random(-1f, 1f), MathUtils.random(-1f, 1f)), MathUtils.random(50, 150), Textures.textureAtlas.createSprite("entity/pumpkin"), 5, Color.WHITE);
            }
        }

        Audio.playSound(Audio.explosion, 0.8f);
    }

    @Override
    public boolean onHit(Entity entity, int health) {
        Level.addParticle(entity, entity.getPosition(), new Vector2(MathUtils.random(-1f,1f), MathUtils.random(-1f,1f)), MathUtils.random(50,150), Textures.textureAtlas.createSprite("entity/particle"), 1, particleColor);
        return true;
    }

    @Override
    public void powerup() {

    }

}
