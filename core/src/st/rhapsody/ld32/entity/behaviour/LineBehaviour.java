package st.rhapsody.ld32.entity.behaviour;

import st.rhapsody.ld32.entity.Entity;
import st.rhapsody.ld32.level.Level;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class LineBehaviour extends EnemyBehaviour{

    @Override
    public void doTick(float delta, Entity entity) {
        super.doTick(delta, entity);

        entity.translate(-(delta * 100f), 0);


    }

    @Override
    public void onDispose(Entity entity) {
        super.onDispose(entity);
        Level.increasePlayerScore(5);
    }
}
