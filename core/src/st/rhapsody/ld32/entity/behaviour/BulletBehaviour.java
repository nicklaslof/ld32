package st.rhapsody.ld32.entity.behaviour;

import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld32.entity.Entity;
import st.rhapsody.ld32.screen.GameScreen;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class BulletBehaviour extends EntityBehaviour {
    private final Vector2 src;
    private final Vector2 direction;
    private final float velocity;
    private final Entity shooter;
    private final boolean enemyBullet;


    public BulletBehaviour(Entity shooter, Vector2 src, Vector2 direction, float velocity, boolean enemyBullet) {
        this.shooter = shooter;
        this.enemyBullet = enemyBullet;
        this.src = src.cpy();

        this.direction = direction.cpy();
        this.velocity = velocity;
    }

    @Override
    public void doTick(float delta, Entity entity) {

        Vector2 position = entity.getPosition();
        if (position.x < 0 || position.x > GameScreen.width || position.y < 0 || position.y > GameScreen.height){
            dispose = true;
            return;
        }


        float shootVelocity = velocity * delta;
        entity.translate(direction.x*shootVelocity, direction.y*shootVelocity);
        entity.rotate(delta*300);
    }

    @Override
    public void onCollision(Entity entity) {
        if ((enemyBullet && !entity.isEnemy()) || (!enemyBullet && entity.isEnemy())) {
            if (!entity.equals(shooter)) {
                entity.hit();
                dispose = true;
            }
        }
    }

    @Override
    public void onDispose(Entity entity) {

    }

    @Override
    public boolean onHit(Entity entity, int health) {
        return true;
    }

    @Override
    public void powerup() {

    }
}
