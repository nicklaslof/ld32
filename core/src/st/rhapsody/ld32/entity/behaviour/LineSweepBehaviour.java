package st.rhapsody.ld32.entity.behaviour;

import com.badlogic.gdx.math.MathUtils;
import st.rhapsody.ld32.entity.Entity;
import st.rhapsody.ld32.level.Level;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class LineSweepBehaviour extends EnemyBehaviour{

    private float timeOffset;
    private float frequency;
    private float time;
    private float bulletTime;
    private float sweepSize;

    public LineSweepBehaviour setTimeOffset(float timeOffset){
        this.timeOffset = timeOffset;
        return this;
    }

    public LineSweepBehaviour setSweepsize(float sweepSize){
        this.sweepSize = sweepSize;
        return this;
    }

    @Override
    public void doTick(float delta, Entity entity) {
        super.doTick(delta, entity);

        frequency = 0.5f;

        time += delta;
        bulletTime -= delta;
        float sin = MathUtils.sin(((time + timeOffset) * MathUtils.PI2) * frequency);

        entity.translate(-(delta * 100f), sin * (delta * sweepSize));


    }

    @Override
    public void onDispose(Entity entity) {
        super.onDispose(entity);
        Level.increasePlayerScore(10);
    }
}
