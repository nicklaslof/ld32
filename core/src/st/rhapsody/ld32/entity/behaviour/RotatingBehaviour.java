package st.rhapsody.ld32.entity.behaviour;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.entity.Entity;
import st.rhapsody.ld32.level.Level;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class RotatingBehaviour extends EnemyBehaviour{

    private float timeOffset;
    private float frequency;
    private float time;
    private float bulletTime;
    private float sweepSize;

    private Vector2 tmpVector = new Vector2();

    public EnemyBehaviour setTimeOffset(float timeOffset){
        this.timeOffset = timeOffset;
        return this;
    }

    public EnemyBehaviour setSweepsize(float sweepSize){
        this.sweepSize = sweepSize;
        return this;
    }

    @Override
    public void doTick(float delta, Entity entity) {
        super.doTick(delta, entity);

        bulletTime -= delta;

        entity.translate(-(delta * 70f), 0);
        entity.rotate(delta*300);

        if (bulletTime <= 0) {
            for (int i = 0; i < 36; i++) {
                tmpVector.set(MathUtils.sin(i*10f), MathUtils.cos(i * 10f));

                Level.shoot(entity, entity.getPosition(), tmpVector.nor(), 300f, Textures.textureAtlas.createSprite("entity/chili"), true);
                bulletTime = 2.500f;
            }

        }
    }

    @Override
    public void onDispose(Entity entity) {
        super.onDispose(entity);
        Level.increasePlayerScore(30);
    }
}