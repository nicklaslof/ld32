package st.rhapsody.ld32.entity.behaviour;

import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld32.entity.Entity;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class ParticleBehaviour extends EntityBehaviour {
    private final Vector2 src;
    private final Vector2 direction;
    private final float velocity;
    private float ttl;
    private final Entity shooter;
    private final boolean enemyBullet;

    public ParticleBehaviour(Entity shooter, Vector2 src, Vector2 direction, float velocity, float ttl) {
        super();
        this.src = src;
        this.direction = direction;
        this.velocity = velocity;
        this.ttl = ttl;
        this.shooter = shooter;
        this.enemyBullet = shooter.isEnemy();
    }

    @Override
    public void doTick(float delta, Entity entity) {
        ttl -= delta;

        if (ttl <= 0){
            entity.setDisposable(true);
            return;
        }

        float particleVelocity = velocity * delta;
        entity.translate(direction.x*particleVelocity, direction.y*particleVelocity);

    }

    @Override
    public void onCollision(Entity entity) {
        if (entity.isEnemy()){
            return;
        }

        if ((enemyBullet && !entity.isEnemy()) || (!enemyBullet && entity.isEnemy())) {
            if (!entity.equals(shooter)) {
                entity.hit();
                dispose = true;
            }
        }

    }

    @Override
    public void onDispose(Entity entity) {

    }

    @Override
    public boolean onHit(Entity entity, int health) {
        return true;
    }

    @Override
    public void powerup() {

    }
}
