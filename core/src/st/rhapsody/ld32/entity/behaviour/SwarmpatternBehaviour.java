package st.rhapsody.ld32.entity.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.entity.Entity;
import st.rhapsody.ld32.level.Level;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class SwarmpatternBehaviour extends EnemyBehaviour {
    private float time;
    private float frequency;
    private Vector2 tmpVector = new Vector2();
    private float bulletTime = MathUtils.random(0,1f);
    private float timeOffset;
    public EntityBehaviour setTimeOffset(float timeOffset){
        this.timeOffset = timeOffset;
        return this;
    }


    @Override
    public void doTick(float delta, Entity entity) {

        super.doTick(delta, entity);

        frequency = 0.5f;

        time += delta;
        bulletTime -= delta;
        float sin = MathUtils.sin(((time + timeOffset) * MathUtils.PI2) * frequency);
        float cos = MathUtils.cos(((time+timeOffset) * MathUtils.PI2) * frequency);

        entity.translate((sin * (delta * 100f)) - (delta * 100f), cos * (delta * 100f));

        Vector2 playerPosition = Level.getPlayerPosition();


        Vector2 dir = tmpVector.set(playerPosition).sub(entity.getPosition());

        if (bulletTime <= 0) {
            Level.shoot(entity, entity.getPosition(), dir.nor(), 300f, Textures.textureAtlas.createSprite("entity/apple"), true);
            bulletTime = 2.500f;
        }


    }

    @Override
    public void onDispose(Entity entity) {
        super.onDispose(entity);
        Level.increasePlayerScore(15);
    }

    @Override
    public SwarmpatternBehaviour setParticleColor(Color particleColor) {
        super.setParticleColor(particleColor);
        return this;
    }
}
