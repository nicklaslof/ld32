package st.rhapsody.ld32;

import com.badlogic.gdx.audio.Sound;

/**
 * Created by nicklaslof on 18/04/15.
 */
public class Audio {
    public static Sound explosion;
    public static Sound hit;
    public static Sound laser;
    public static Sound powerup;
    public static Sound music;

    public static boolean musicPlaying = true;
    public static boolean soundPlaying = true;

    public static void init(Sound explosion, Sound hit, Sound laser, Sound powerup, Sound music) {
        Audio.explosion = explosion;
        Audio.hit = hit;
        Audio.laser = laser;
        Audio.powerup = powerup;
        Audio.music = music;
    }

    public static void switchMode() {
        if (musicPlaying){
            musicPlaying = false;
            music.stop();
        }else if (soundPlaying){
            soundPlaying = false;
        }else{
            soundPlaying = true;
            musicPlaying = true;
            music.loop();
        }

    }

    public static void playSound(Sound sound, float volume) {
        if (soundPlaying){
            sound.play(volume);
        }
    }
}
