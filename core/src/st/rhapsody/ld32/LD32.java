package st.rhapsody.ld32;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import st.rhapsody.ld32.data.Textures;
import st.rhapsody.ld32.screen.EndScreen;
import st.rhapsody.ld32.screen.GameScreen;
import st.rhapsody.ld32.screen.IntroScreen;
import st.rhapsody.ld32.screen.MenuScreen;

public class LD32 extends Game {

	private AssetManager assetManager;
	private Screen currentScreen;

	@Override
	public void create() {

		assetManager = new AssetManager();
		assetManager.load("data/textureatlas.atlas", TextureAtlas.class);
		assetManager.load("data/starfield.png", Texture.class);
		assetManager.load("data/ld32.ogg", Sound.class);

		assetManager.load("data/explosion.ogg", Sound.class);
		assetManager.load("data/hit.ogg", Sound.class);
		assetManager.load("data/laser.ogg", Sound.class);
		assetManager.load("data/powerup.ogg", Sound.class);

		assetManager.finishLoading();

		Textures.init(assetManager.get("data/textureatlas.atlas", TextureAtlas.class), assetManager.get("data/starfield.png", Texture.class));
		Audio.init(
				assetManager.get("data/explosion.ogg", Sound.class),
				assetManager.get("data/hit.ogg", Sound.class),
				assetManager.get("data/laser.ogg", Sound.class),
				assetManager.get("data/powerup.ogg", Sound.class),
				assetManager.get("data/ld32.ogg", Sound.class));

		Audio.music.loop();

		//setScreen(new GameScreen());
		switchToMenu();
	}

	public void switchToMenu() {
		disposeCurrentScreen();
		currentScreen = new MenuScreen(this);
		setScreen(currentScreen);
	}

	private void disposeCurrentScreen() {
		if (currentScreen != null){
			currentScreen.dispose();
		}
	}

	public void switchToIntro() {
		disposeCurrentScreen();
		currentScreen = new IntroScreen(this);
		setScreen(currentScreen);
	}

	public void switchToGame(boolean freeplay) {
		disposeCurrentScreen();
		currentScreen = new GameScreen(this,freeplay);
		setScreen(currentScreen);
	}

	public void switchToEnd() {
		disposeCurrentScreen();
		currentScreen = new EndScreen(this);
		setScreen(currentScreen);
	}
}
